/**
 * Created by renato.oliveira on 17/05/2016.
 */
Meteor.publish("doctors",function (options) {
    var opt = {
        limit: 10
    };
    if (options) {
        if (Number(options.skip) > 0) {
            opt.skip = Number(options.skip);
        }
    }
    return Doctor.find({}, opt);

});