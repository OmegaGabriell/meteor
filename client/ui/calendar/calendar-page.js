/**
 * Created by renato.oliveira on 17/05/2016.
 */
Template.calendarPage.onRendered(function(){



        $('#calendar').fullCalendar({
            defaultView: 'month',
            events: [],
            resources: [
                { id: 'a', title: 'Room A' },
                { id: 'b', title: 'Room B' },
                { id: 'c', title: 'Room C' },
                { id: 'd', title: 'Room D' }
            ]
        });

    Tracker.autorun(function(){
        var events = Events.find().fetch();
        console.log(events)
        $('#calendar').fullCalendar( 'removeEvents' )
        $('#calendar').fullCalendar( 'addEventSource', events )
     //   $('#calendar').fullCalendar(   events:events);

    });
});

