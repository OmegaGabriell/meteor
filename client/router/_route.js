/**
 * Created by renato.oliveira on 17/05/2016.
 */
Router.configure({
    layoutTemplate: 'appLayout',

});

BaseController = RouteController.extend({
    layoutTemplate: 'appLayout',
    onBeforeAction: function () {
        if (!Meteor.user()){
            Router.go("home");

        }
        this.next();
    },

})


Router.map(function(){
    this.route("home",{
        path: '/'
    });

    this.route("insertDoctorForm",{
        path: '/doctor/add',
        controller: "BaseController",
    });

    this.route("updateDoctorForm",{
        path: '/doctor/edit/:_id',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
            console.log(this.params)
            return Doctor.findOne({_id: this.params._id})
        }
    });

    this.route("doctorListPage",{
        path: '/doctors',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
        }
    });



    this.route("insertPacientForm",{
        path: '/pacient/add',
        controller: "BaseController",
    });

    this.route("updatePacientForm",{
        path: '/pacient/edit/:_id',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
            console.log(this.params)
            return Pacient.findOne({_id: this.params._id})
        }
    });
    this.route("pacientListPage",{
        path: '/pacients',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
        }
    });


    this.route("insertAtendenteForm",{
        path: '/atendente/add',
        controller: "BaseController",
    });

    this.route("updateAtendenteForm",{
        path: '/atendente/edit/:_id',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
            console.log(this.params)
            return Atendente.findOne({_id: this.params._id})
        }
    })
    this.route("adentendesListPage",{
        path: '/atendentes',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
        }
    });

    this.route("calendarPage",{
        path: '/calendar',
        controller: "BaseController",
        waitOn: function(){
        },
        data: function(){
        }
    });

});