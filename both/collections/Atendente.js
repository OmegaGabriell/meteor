/**
 * Created by renato.oliveira on 17/05/2016.
 */
Atendente = new Meteor.Collection("atendente");


Atendente.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Nome",
        max: 200
    },
    cpf: {
        type: String,
        label: "CPF"
    },
    phone: {
        type: Number,
        label: "telefone",
        optional: true
    },
    address: {
        type: String,
        label: "Endereco",
        optional: true
    },
    email: {
        type: String,
        label: "Email",
        optional: true
    }
}));
