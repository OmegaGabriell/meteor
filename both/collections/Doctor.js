/**
 * Created by renato.oliveira on 17/05/2016.
 */
Doctor = new Meteor.Collection("doctors");

Doctor.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Nome",
        max: 200
    },
    byrthday: {
        type: Date,
        label: "Nascimento"
    },
    cpf: {
        type: Number,
        label: "CPF",
        min: 0
    },
    phone: {
        type: Number,
        label: "telefone",
        optional: true
    },
    address: {
        type: String,
        label: "Endereco",
        optional: true,
        max: 1000
    },
    email: {
        type: String,
        label: "Email",
        optional: true,

    },
    crm: {
        type: Number,
        label: "CRM",
        optional: true,

    }
}));
