/**
 * Created by renato.oliveira on 17/05/2016.
 */
Pacient = new Meteor.Collection('pacients')


Pacient.attachSchema(new SimpleSchema({
    name: {
        type: String,
        label: "Nome",
        max: 200
    },
    byrthday: {
        type: Date,
        label: "Nascimento"
    },
    cpf: {
        type: Number,
        label: "CPF",
        min: 0
    },
    phone: {
        type: Number,
        label: "telefone",
        optional: true
    },
    address: {
        type: String,
        label: "Endereco",
        optional: true,
        max: 1000
    },
    email: {
        type: String,
        label: "Email",
        optional: true,
        max: 1000
    },
    responsable: {
        type: String,
        label: "Responsavel",
        optional: true,
        max: 1000
    },
    responsable_phone: {
        type: String,
        label: "Telefone Responsavel",
        optional: true,
    },
    atendimento_typo:{
        type: String,
        label: "Tipo de atendimento",
        optional: true,
    },
    convenio:{
        type: String,
        label: "Convenio",
        optional: true,
    },
    indicacao_profissional:{
        type: String,
        label: "Indicação profissional",
        optional: true,
    },
    observations:{
        type: String,
        label: "Observações",
        optional: true,
    },
}));
